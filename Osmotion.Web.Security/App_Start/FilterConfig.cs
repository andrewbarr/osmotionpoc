﻿using System.Web;
using System.Web.Mvc;

namespace Osmotion.Web.Security
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}